package info.hccis.cis2250.canescamper.ui.camperList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CamperListViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CamperListViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("This is Camper List fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}