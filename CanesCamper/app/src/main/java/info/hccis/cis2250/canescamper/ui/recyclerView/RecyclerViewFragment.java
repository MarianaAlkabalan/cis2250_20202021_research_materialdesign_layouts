package info.hccis.cis2250.canescamper.ui.recyclerView;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import info.hccis.cis2250.canescamper.R;


public class RecyclerViewFragment extends Fragment {

    RecyclerView recyclerView;

    String campers[];
    String level[];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);

        campers = getResources().getStringArray(R.array.CamperNames);
        level = getResources().getStringArray(R.array.camperType);

        MyAdapter myAdapter = new MyAdapter(recyclerView.getContext(), campers, level);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));

        // Inflate the layout for this fragment
        return view;
    }
}