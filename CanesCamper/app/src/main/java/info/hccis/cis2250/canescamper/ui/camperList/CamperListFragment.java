package info.hccis.cis2250.canescamper.ui.camperList;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import info.hccis.cis2250.canescamper.R;
import info.hccis.cis2250.canescamper.ui.feedback.FeedbackViewModel;

public class CamperListFragment extends Fragment {

    private CamperListViewModel camperListViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        camperListViewModel =
                new ViewModelProvider(this).get(CamperListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_camper_list, container, false);
        //final TextView textView = root.findViewById(R.id.text_feedback);
        camperListViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        return root;
    }
}