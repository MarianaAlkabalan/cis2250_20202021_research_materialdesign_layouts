# CIS2250 Project #

### What is this repository for? ###

* This repository contains Material Design & Layouts research for cis2250
* 2020/2021 AYR

### Who do I talk to? ###

Learning Manager BJ MacLean

Email: bjmaclean@hollandcollege.com

Mariana Alkabalan

Email: malkabalan@hollandcollege.com

Nidhiya Nair

Email: nnair@hollandcollege.com

Megha Megha

Email: mmegha@hollandcollege.com

Logan MacKinnon

Email: lmackinnon122691@hollandcollege.com
